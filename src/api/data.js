import request from '@/utils/request'

export function postRequest(url, data) {
  return request({
    url: url,
    method: 'post',
    data
  })
}

export function postRequestDownload(url, data, fileName) {
  return request({
    url: url,
    method: 'post',
    data,
    fileName: fileName,
    responseType: 'blob' // 表明返回服务器返回的数据类型
  })
}
